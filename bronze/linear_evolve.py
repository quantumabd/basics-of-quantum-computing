
def linear_evolve(A,si):
    so = []
    for row in A:
        so.append(sum([round(row[i]*si[i],4) for i in range(len(row))]))
    return so    
